<?php

use Aedart\Laravel\Config\Interfaces\ConfigAware;
use Aedart\Laravel\Config\Traits\ConfigTrait;
use Aedart\Testing\Laravel\Traits\ApplicationInitiatorTrait;
use Illuminate\Config\Repository as ConfigRepository;
use Illuminate\Contracts\Config\Repository;
use Faker\Factory as FakerFactory;

/**
 * Class ConfigTraitTest
 *
 * @coversDefaultClass Aedart\Laravel\Config\Traits\ConfigTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class ConfigTraitTest extends \Codeception\TestCase\Test
{
    use ApplicationInitiatorTrait;

    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Faker
     *
     * @var \Faker\Generator
     */
    protected $faker = null;

    protected function _before()
    {
        $this->faker = FakerFactory::create();
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Helpers
     *****************************************************************************/

    /**
     * Get mock for given trait
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\Laravel\Config\Interfaces\ConfigAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Laravel\Config\Traits\ConfigTrait');
        return $m;
    }

    /**
     * Get a dummy implementation
     *
     * @return Aedart\Laravel\Config\Interfaces\ConfigAware
     */
    protected function getDummyImpl(){
        return new DummyConfigClass();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::hasDefaultConfig
     * @covers ::getDefaultConfig
     */
    public function getNullDefaultConfig(){
        $dummy = $this->getDummyImpl();
        $this->assertFalse($dummy->hasDefaultConfig());
        $this->assertNull($dummy->getDefaultConfig());
    }

    /**
     * @test
     * @covers ::hasDefaultConfig
     * @covers ::getConfig
     * @covers ::hasConfig
     * @covers ::setConfig
     * @covers ::isConfigValid
     * @covers ::getDefaultConfig
     */
    public function getEmptyRepositoryAsDefaultConfig(){
        $trait = $this->getTraitMock();
        $this->assertTrue($trait->hasDefaultConfig());
        $this->assertInstanceOf('Illuminate\Contracts\Config\Repository', $trait->getConfig());
        $this->assertFalse($trait->getConfig()->has('database'), 'Empty configuration repository expected');
    }

    /**
     * @test
     * @covers ::hasDefaultConfig
     * @covers ::getConfig
     * @covers ::hasConfig
     * @covers ::setConfig
     * @covers ::isConfigValid
     * @covers ::getDefaultConfig
     */
    public function getLaravelConfigAsDefaultConfig(){
        $this->startApplication();

        $trait = $this->getTraitMock();
        $this->assertTrue($trait->hasDefaultConfig());
        $this->assertInstanceOf('Illuminate\Contracts\Config\Repository', $trait->getConfig());
        $this->assertTrue($trait->getConfig()->has('database'), 'Configuration repository should NOT be empty!');

        $this->stopApplication();
    }

    /**
     * @test
     * @covers ::getConfig
     * @covers ::hasConfig
     * @covers ::setConfig
     * @covers ::isConfigValid
     */
    public function setAndGetConfig(){
        $trait = $this->getTraitMock();

        $list = [
            $this->faker->unique()->word => $this->faker->city,
            $this->faker->unique()->word => $this->faker->address,
            $this->faker->unique()->word => $this->faker->name,
        ];
        $repository = new ConfigRepository($list);

        $trait->setConfig($repository);

        foreach($list as $key => $value){
            $this->assertTrue($trait->getConfig()->has($key), sprintf('%s (key) does not exist in configuration', $key));
            $this->assertSame($value, $trait->getConfig()->get($key) , sprintf('%s (value) does not match!', $value));
        }
    }

    /**
     * @test
     * @covers ::setConfig
     * @covers ::isConfigValid
     *
     * @expectedException \Aedart\Laravel\Config\Exceptions\InvalidConfigException
     */
    public function attemptSetInvalidConfig(){
        $dummy = $this->getDummyImpl();

        $list = [
            $this->faker->unique()->word => $this->faker->city,
            $this->faker->unique()->word => $this->faker->address,
            $this->faker->unique()->word => $this->faker->name,
        ];
        $repository = new ConfigRepository($list);

        $dummy->setConfig($repository);
    }
}

class DummyConfigClass implements ConfigAware {
    use ConfigTrait;

    public function getDefaultConfig() {
        return null;
    }


    public function isConfigValid(Repository $repository) {
        return false;
    }
}