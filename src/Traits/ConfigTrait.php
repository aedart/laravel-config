<?php  namespace Aedart\Laravel\Config\Traits;

use Aedart\Laravel\Config\Exceptions\InvalidConfigException;
use Aedart\Laravel\Detector\ApplicationDetector;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Config\Repository as ConfigurationRepository;

/**
 * Trait Config
 *
 * @see \Aedart\Laravel\Config\Interfaces\ConfigAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Config\Traits
 */
trait ConfigTrait {

    /**
     * This component's configuration repository
     *
     * @var \Illuminate\Contracts\Config\Repository|null
     */
    protected $config = null;

    /**
     * Set the configuration repository
     *
     * @param ConfigurationRepository $repository This components configuration repository or null if none has been set
     *
     * @return void
     *
     * @throws InvalidConfigException If the given repository is invalid, e.g. contains or doesn't contain
     *                                  certain entries / values
     */
    public function setConfig(ConfigurationRepository $repository){
        if(!$this->isConfigValid($repository)){
            throw new InvalidConfigException(sprintf('The given configuration repository is invalid; ', var_export($repository, true)));
        }
        $this->config = $repository;
    }

    /**
     * Get the configuration repository
     *
     * If no repository has been set, this method sets and returns
     * a default repository, if any is available
     *
     * @see getDefaultConfig()
     *
     * @return \Illuminate\Contracts\Config\Repository|null This components configuration repository or null if none has been set
     */
    public function getConfig(){
        if(!$this->hasConfig() && $this->hasDefaultConfig()){
            $this->setConfig($this->getDefaultConfig());
        }
        return $this->config;
    }

    /**
     * Get a default repository, if any is available
     *
     * @return \Illuminate\Contracts\Config\Repository|null A repository instance or null if no default is available
     */
    public function getDefaultConfig(){
        $detector = new ApplicationDetector();
        if($detector->isApplicationAvailable()){
            return app('config');
        }
        return new Repository();
    }

    /**
     * Check if a repository has been set
     *
     * @return bool True if a repository has been set, false if not
     */
    public function hasConfig(){
        if(!is_null($this->config)){
            return true;
        }
        return false;
    }

    /**
     * Check if a default repository is available
     *
     * @return bool True if a default repository is available, false if not
     */
    public function hasDefaultConfig(){
        if(!is_null($this->getDefaultConfig())){
            return true;
        }
        return false;
    }

    /**
     * Check if the given repository is valid, e.g. if specific entries are within the
     * given repository.
     *
     * @param \Illuminate\Contracts\Config\Repository $repository The repository to be validated
     *
     * @return bool True if the given repository is valid, e.g. contains specific entries, with eventual
     *              specific values, ...etc. False if the repository is invalid
     */
    public function isConfigValid(ConfigurationRepository $repository){
        // By default, no validation is performed - this should
        // always be overwritten in concrete situations, if specific
        // validation is required, such as ensuring certain repository
        // entries / values are set or exist
        return true;
    }

}