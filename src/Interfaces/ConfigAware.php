<?php  namespace Aedart\Laravel\Config\Interfaces;

use Aedart\Laravel\Config\Exceptions\InvalidConfigException;
use Illuminate\Contracts\Config\Repository as ConfigurationRepository;

/**
 * Interface Config Aware
 *
 * Components that implement this, promise that a configuration repository can be specified
 * and obtained again, when it is needed. Furthermore, depending upon implementation, a
 * default repository might be available, if none has been specified prior to obtaining
 * it.
 *
 * @see \Illuminate\Contracts\Config\Repository
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Config\Interfaces
 */
interface ConfigAware {

    /**
     * Set the configuration repository
     *
     * @param ConfigurationRepository $repository This components configuration repository or null if none has been set
     *
     * @return void
     *
     * @throws InvalidConfigException If the given repository is invalid, e.g. contains or doesn't contain
     *                                  certain entries / values
     */
    public function setConfig(ConfigurationRepository $repository);

    /**
     * Get the configuration repository
     *
     * If no repository has been set, this method sets and returns
     * a default repository, if any is available
     *
     * @see getDefaultConfig()
     *
     * @return \Illuminate\Contracts\Config\Repository|null This components configuration repository or null if none has been set
     */
    public function getConfig();

    /**
     * Get a default repository, if any is available
     *
     * @return \Illuminate\Contracts\Config\Repository|null A repository instance or null if no default is available
     */
    public function getDefaultConfig();

    /**
     * Check if a repository has been set
     *
     * @return bool True if a repository has been set, false if not
     */
    public function hasConfig();

    /**
     * Check if a default repository is available
     *
     * @return bool True if a default repository is available, false if not
     */
    public function hasDefaultConfig();

    /**
     * Check if the given repository is valid, e.g. if specific entries are within the
     * given repository.
     *
     * @param \Illuminate\Contracts\Config\Repository $repository The repository to be validated
     *
     * @return bool True if the given repository is valid, e.g. contains specific entries, with eventual
     *              specific values, ...etc. False if the repository is invalid
     */
    public function isConfigValid(ConfigurationRepository $repository);
}