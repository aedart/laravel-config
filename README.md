## Laravel-Config ([Deprecated](https://en.wikipedia.org/wiki/Deprecation), use [Aedart/Laravel-Helpers](https://bitbucket.org/aedart/laravel-helpers) instead) ##

Getter and Setter package for a Laravel configuration repository instance. It serves as an alternative to the `Config` facade.

## Contents ##

[TOC]

## When to use this ##

When your components needs to be make us of some kind of configuration. Or if you component needs to make use of Laravel's specific configuration. 

## How to install ##

For Laravel version 5.0.x

```
#!console

composer require aedart/laravel-config 1.0.*
```

This package uses [composer](https://getcomposer.org/). If you do not know what that is or how it works, I recommend that you read a little about, before attempting to use this package.

## Quick start ##

Provided that you have an interface, e.g. for a command, you can extend the config-aware interface;

```
#!php
<?php
use Aedart\Laravel\Config\Interfaces\ConfigAware;

interface ICommand extends ConfigAware {

    // ... Remaining interface implementation not shown ...
    
}

```

In your concrete implementation, you simple use the config-traits;
 
```
#!php
<?php
use Aedart\Laravel\Config\Traits\ConfigTrait;

class DoSomethingCommand implements ICommand {
 
    use ConfigTrait;

    // ... Remaining implementation not shown ... 
 
}
```

### Default Configuration Repository instance ###

The `ConfigTrait` will by default return the current running Laravel configuration repository instance, if any is available, and provided that your component is running inside a Laravel application.

However, if none is available (not running inside a Laravel Application), a default empty `Illuminate\Contracts\Config\Repository` instance is returned instead.

### Default Configuration Repository Validation ###

By default, the `ConfigTrait` will always assume that the specified configuration repository instance is valid. However, if you need to ensure that specific configuration has been set or
certain entries are available, or some other kind of configuration-specific validation, then you can do so, by overriding the `isConfigValid()` method.

```
#!php
<?php
use Aedart\Laravel\Config\Traits\ConfigTrait;
use Illuminate\Contracts\Config\Repository;

class DoSomethingCommand implements ICommand {
 
    use ConfigTrait;

    public function isConfigValid(Repository $repository){
        // In this example, a configuration repository is only valid
        // if there is a database.default entry
        return $repository->has('database.default'));
    }

    // ... Remaining implementation not shown ... 
 
}
```

## Acknowledgement ##

[Taylor Otwell et al.](http://laravel.com/) for one of the best PHP frameworks ever created.

## License ##

[BSD-3-Clause](http://spdx.org/licenses/BSD-3-Clause), Read the LICENSE file included in this package