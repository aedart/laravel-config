<?php  namespace Aedart\Laravel\Config\Exceptions; 

/**
 * Class Invalid Config Exception
 *
 * Throw this exception when an invalid configuration repository has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Config\Exceptions
 */
class InvalidConfigException extends \InvalidArgumentException{

}